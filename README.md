# AUR Package Updates

## Files

* `archversion.conf`: Non-VCS packages go in here. The URL provided should lead to a page with releases, and the regular expression should parse the release with brackets around the portion matching the version.
* `develversion.conf`: VCS packages go in here. Their `pkgver` function is used to determine whether they have updates.
* `noversion.txt`: Packages that shouldn't be checked for updates go in here.
* `checkversion`: Run this script to check for updates.

## Setup

1. Build and install [archversion-envconfig-git](https://github.com/ArchStrike/archversion-envconfig-git)
2. Clone this repo as `.updates` inside the directory containing your AUR packages and modify `archversion.conf`, `develversion.conf` and `noversion.txt`
3. Create a script that changes to this repository's directory and runs `checkversion`

## Screenshot

![Screenshot of the checker being run](./aur-package-updates.png)
